Test project for Active Withards

#Install components

1. Install MongoDB
2. sudo service mongod start
3. run: mongo
4. in shell: use tutorial
5. quit()


#Prepare envirement

1. Create directory
2. cd {diectory}
3. Clone repository - run command: git clone git@gitlab.com:igorfiv/activewizards-demo-project.git
4. Create virtual environment: virtualenv my_project
5. source my_project/bin/activate
6. pip install requirements
7. cd activewizards-demo-project
8. Run command to import sample data: mongoimport --d tutorial --c world_bank --file=world_bank.json


#Start server

1. export FLASK_APP=world.py
2. flask run
3. Open in browser